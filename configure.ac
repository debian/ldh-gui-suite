dnl -*- mode: autoconf; coding: utf-8 -*-

AC_COPYRIGHT(
[Copyright 2019, Jonas Smedegaard <dr@jones.dk>
Copyright 2019, Purism, SPC

This program is free software;
you can redistribute it and/or modify it
under the terms of the Affero GNU General Public License
as published by the Free Software Foundation;
either version 3, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.
If not, see <http://www.gnu.org/licenses/>.])

dnl Build-Depends: autoconf-archive (>= 20150224), git

AC_PREREQ([2.69])
AC_INIT(
	m4_esyscmd_s([grep --max-count=1 --only-matching --perl-regexp --regexp='^\w\S*( \S+)*' README.md]),
	[0.1],
	m4_esyscmd_s([grep --max-count=1 --only-matching --perl-regexp --regexp='\[tracker\]: \K\S+' README.md]),
	[],
	m4_esyscmd_s([grep --max-count=1 --only-matching --perl-regexp --regexp='\[project\]: \K\S+' README.md]))

AC_CONFIG_AUX_DIR([build-config])
AM_INIT_AUTOMAKE([no-define foreign])
m4_ifdef([AM_SILENT_RULES],[AM_SILENT_RULES([yes])])
AM_MAINTAINER_MODE([enable])

AC_PROG_MKDIR_P
AX_WITH_PROG(SCOUR,scour)
AX_WITH_PROG(RSVG_CONVERT,rsvg-convert)
AX_WITH_PROG(GTK_UPDATE_ICON_CACHE,gtk-update-icon-cache)

m4_define([_DOMAIN_DEFAULT],[librem.one])
AC_ARG_ENABLE([domain],
	[AS_HELP_STRING([--enable-domain=DOMAIN],
		[set custom Liberty Deck Host domain, or disable installing desktop files altogether @<:@default=]_DOMAIN_DEFAULT[@:>@])],
	[],
	[enable_domain="]_DOMAIN_DEFAULT["])

DOMAIN=
AS_IF([test "x$enable_domain" != xno],
	[AC_DEFINE([HAVE_DOMAIN], [1])
		AS_IF([test "x$enable_domain" != xyes],
			[AC_SUBST([DOMAIN], ["$enable_domain"])],
			[AC_SUBST([DOMAIN], ["]_DOMAIN_DEFAULT["])])],
	[AC_DEFINE([HAVE_NO_DOMAIN], [1],
		[Define that no Liberty Deck Host domain is set])])
AM_CONDITIONAL([HAVE_DOMAIN], [test "x$enable_domain" != xno])

m4_define([_HUB_NAME_DEFAULT],[Liberty One])
AC_ARG_ENABLE([hub-name],
	[AS_HELP_STRING([--enable-hub-name=NAME],
		[set name for Liberty Deck Host service Hub @<:@default=]_HUB_NAME_DEFAULT[@:>@])],
	[],
	[enable_hub_name="]_HUB_NAME_DEFAULT["])

HUB_NAME=
AS_IF([test "x$enable_hub_name" != xno],
	[AS_IF([test "x$enable_hub_name" != xyes],
		[AC_SUBST([HUB_NAME], ["$enable_hub_name"])],
		[AC_SUBST([HUB_NAME], ["]_HUB_NAME_DEFAULT["])])])

m4_define([_HUB_URI_DEFAULT],[https://social.DOMAIN/web/getting-started])
AC_ARG_ENABLE([hub-uri],
	[AS_HELP_STRING([--enable-hub-uri=URI],
		[set URI for Liberty Deck Host service Hub, expanding DOMAIN to Liberty Deck Host domain @<:@default=]_HUB_URI_DEFAULT[@:>@])],
	[],
	[enable_hub_uri="]_HUB_URI_DEFAULT["])

HUB_URI=
AS_IF([test "x$enable_hub_uri" != xno],
	[AS_IF([test "x$enable_hub_uri" != xyes],
		[AC_SUBST([HUB_URI], [`echo "$enable_hub_uri" | sed "s|DOMAIN|$DOMAIN|"`])],
		[AC_SUBST([HUB_URI], [`echo "]_HUB_URI_DEFAULT[" | sed "s|DOMAIN|$DOMAIN|"`])])])

m4_define([_SOCIAL_URI_DEFAULT],[https://social.DOMAIN/web/getting-started])
AC_ARG_ENABLE([social-uri],
	[AS_HELP_STRING([--enable-social-uri=URI],
		[set URI for Liberty Deck Host service Social, expanding DOMAIN to Liberty Deck Host domain @<:@default=]_SOCIAL_URI_DEFAULT[@:>@])],
	[],
	[enable_social_uri="]_SOCIAL_URI_DEFAULT["])

SOCIAL_URI=
AS_IF([test "x$enable_social_uri" != xno],
	[AS_IF([test "x$enable_social_uri" != xyes],
		[AC_SUBST([SOCIAL_URI], [`echo "$enable_social_uri" | sed "s|DOMAIN|$DOMAIN|"`])],
		[AC_SUBST([SOCIAL_URI], [`echo "]_SOCIAL_URI_DEFAULT[" | sed "s|DOMAIN|$DOMAIN|"`])])])

AX_GENERATE_CHANGELOG

AC_CONFIG_FILES([Makefile])
AS_IF([test "x$DOMAIN" != x],
	[AC_CONFIG_FILES(
		[data/one.liberty.hub.desktop:templates/one.liberty.hub.desktop.in]
		[data/one.liberty.social.desktop:templates/one.liberty.social.desktop.in])])
AC_OUTPUT
